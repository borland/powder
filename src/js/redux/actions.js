import { gnarActions } from 'gnar-edge/es/redux';

export const GET_ACCOUNT_DETAILS = 'GET_ACCOUNT_DETAILS';
export const GET_PASSWORD_SCORE = 'GET_PASSWORD_SCORE';
export const RESET_PASSWORD = 'RESET_PASSWORD';
export const SEND_RESET_PASSWORD_EMAIL = 'SEND_RESET_PASSWORD_EMAIL';
export const SET_ACCOUNT_DETAILS = 'SET_ACCOUNT_DETAILS';
export const SET_LANGUAGE = 'SET_LANGUAGE';
export const SET_PASSWORD = 'SET_PASSWORD';
export const SUBMIT_ACTIVATION = 'SUBMIT_ACTIVATION';
export const SUBMIT_LOGIN = 'SUBMIT_LOGIN';
export const SUBMIT_LOGOUT = 'SUBMIT_LOGOUT';
export const SUBMIT_SIGNUP_EMAIL = 'SUBMIT_SIGNUP_EMAIL';

export default gnarActions({
  activateActions: {
    [SUBMIT_ACTIVATION]: 'token'
  },
  accountDetailsActions: {
    [GET_ACCOUNT_DETAILS]: 'email',
    [SET_ACCOUNT_DETAILS]: [ 'userId', 'data' ]
  },
  languageActions: {
    [SET_LANGUAGE]: 'current'
  },
  loginActions: {
    [SUBMIT_LOGIN]: [ 'email', 'password', 'reCaptchaResponse' ]
  },
  logoutActions: {
    [SUBMIT_LOGOUT]: []
  },
  passwordActions: {
    [GET_PASSWORD_SCORE]: [ 'password', 'passwordScoreDispatchedAt' ],
    [RESET_PASSWORD]: [ 'actionId', 'email', 'password' ],
    [SET_PASSWORD]: [ 'userId', 'password' ]
  },
  sendResetPasswordEmailActions: {
    [SEND_RESET_PASSWORD_EMAIL]: [ 'email', 'reCaptchaResponse' ]
  },
  signupActions: {
    [SUBMIT_SIGNUP_EMAIL]: [ 'email', 'reCaptchaResponse' ]
  }
});
