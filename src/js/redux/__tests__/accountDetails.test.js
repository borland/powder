import { drain } from 'gnar-edge';

import { ConsoleMock, FetchMock, LocalStorageMock, withAuthRequestHeader } from 'mocks';
import { ExpiredTokenError } from 'errors';
import { accountDetails, userId, email, expiredJwt, jwt } from 'mocks/constants';
import actions from 'actions';
import configureStore from 'js/redux/configureStore';

const { accountDetailsActions } = actions;
const notificationsSagas = require('gnar-edge/es/notifications/redux/sagas');

notificationsSagas.notifyError = jest.fn();

describe('redux accountDetails', () => {
  it('properly updates the store on successful getAccountDetails', drain(function* () {
    const store = configureStore();
    const fetchMock = new FetchMock(accountDetails, { withAuthResponseHeader: true });
    const localStorageMock = new LocalStorageMock();
    localStorage.setItem('jwt', jwt);
    yield store.dispatch(accountDetailsActions.getAccountDetails(email));
    const body = JSON.stringify({ email });
    expect(fetchMock.fn)
      .toHaveBeenCalledWith('/user/get', withAuthRequestHeader({ method: 'POST', body }));
    expect(store.getState().accountDetails.toJSON()).toEqual(accountDetails);
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));

  it('submits the correct request on setAccountDetails', drain(function* () {
    const store = configureStore();
    const fetchMock = new FetchMock({ accountDetails }, { withAuthResponseHeader: true });
    const localStorageMock = new LocalStorageMock();
    localStorage.setItem('jwt', jwt);
    yield store.dispatch(accountDetailsActions.setAccountDetails(userId, accountDetails));
    const body = JSON.stringify(accountDetails);
    expect(fetchMock.fn)
      .toHaveBeenCalledWith(`/user/${userId}`, withAuthRequestHeader({ method: 'PUT', body }));
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));

  it('does nothing on setAccountDetails without a userId', drain(function* () {
    const store = configureStore();
    const fetchMock = new FetchMock();
    yield store.dispatch(accountDetailsActions.setAccountDetails(null, accountDetails));
    expect(fetchMock.fn).not.toHaveBeenCalled();
    fetchMock.deactivate();
  }));

  it('throws an ExpiredTokenError on setAccountDetails when the jwt token has expired', drain(function* () {
    const historyMock = { replace: jest.fn() };
    const store = configureStore(historyMock);
    const consoleMock = new ConsoleMock();
    const fetchMock = new FetchMock({ accountDetails }, { withAuthResponseHeader: true });
    const localStorageMock = new LocalStorageMock();
    localStorage.setItem('jwt', expiredJwt);
    yield store.dispatch(accountDetailsActions.setAccountDetails(userId, accountDetails));
    expect(consoleMock.error).toHaveBeenCalledWith(new ExpiredTokenError());
    expect(notificationsSagas.notifyError).toHaveBeenCalledWith(new ExpiredTokenError().message);
    expect(localStorage.clear).toHaveBeenCalled();
    expect(historyMock.replace).toHaveBeenCalledWith('/login');
    consoleMock.deactivate();
    fetchMock.deactivate();
    localStorageMock.deactivate();
  }));

  it(
    'throws an ExpiredTokenError on setAccountDetails when the jwt is missing in the response header',
    drain(function* () {
      const historyMock = { replace: jest.fn() };
      const store = configureStore(historyMock);
      const consoleMock = new ConsoleMock();
      const fetchMock = new FetchMock({ accountDetails });
      const localStorageMock = new LocalStorageMock();
      localStorage.setItem('jwt', jwt);
      yield store.dispatch(accountDetailsActions.setAccountDetails(userId, accountDetails));
      expect(consoleMock.error).toHaveBeenCalledWith(new ExpiredTokenError());
      expect(notificationsSagas.notifyError).toHaveBeenCalledWith(new ExpiredTokenError().message);
      expect(localStorage.clear).toHaveBeenCalled();
      expect(historyMock.replace).toHaveBeenCalledWith('/login');
      consoleMock.deactivate();
      fetchMock.deactivate();
      localStorageMock.deactivate();
    })
  );
});
