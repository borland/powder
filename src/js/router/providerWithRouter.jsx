import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';
import withRouter from 'react-router-dom/withRouter';

import configureStore from 'js/redux/configureStore';

@withRouter
export default class ProviderWithRouter extends Component {
  store = configureStore(this.props.history);

  static propTypes = {
    children: PropTypes.node.isRequired,
    history: ReactRouterPropTypes.history.isRequired
  }

  render() {
    return (
      <Provider store={this.store}>
        {this.props.children}
      </Provider>
    );
  }
}
