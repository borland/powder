import { getJwt, isLoggedIn } from 'gnar-edge/es/jwt';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import Redirect from 'react-router-dom/Redirect';
import Route from 'react-router-dom/Route';

export default class AuthenticatedRoute extends PureComponent {
  static propTypes = {
    viewName: PropTypes.string.isRequired
  }

  state = { RouteComponent: null };

  componentDidMount() {
    const { viewName } = this.props;
    import(/* webpackChunkName: "views/[request]" */ `../views/dynamic/${viewName}`)
      .then(({ [viewName]: RouteComponent }) => { this.setState({ RouteComponent }); });
  }

  render() {
    const { viewName, ...rest } = this.props;
    const { RouteComponent } = this.state;
    return (
      <Route
        {...rest}
        render={props => (
          isLoggedIn()
            ? RouteComponent
              ? <RouteComponent email={getJwt().identity} {...props} />
              : null
            : <Redirect to='/login' />
        )}
      />
    );
  }
}
