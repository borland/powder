import React from 'react';
import { mount } from 'enzyme';

import { ENGLISH, english } from 'i18n';
import AccountDetailsStep3 from 'views/accountDetails/step3';

const { accountDetails: { step3Content } } = english;

const getRootComponent = () => mount(<AccountDetailsStep3 language={ENGLISH} />);

describe('<AccountDetailsStep3 />', () => {
  it('renders properly', () => {
    const root = getRootComponent();
    expect(root.find('Grid Grid[item=true]').map(el => el.text())).toEqual(step3Content);
  });
});
