import React from 'react';
import { mount } from 'enzyme';

import { ENGLISH, english } from 'i18n';
import AccountDetailsStep2 from 'views/accountDetails/step2';

const { accountDetails: { step2Content } } = english;

const getRootComponent = () => mount(<AccountDetailsStep2 language={ENGLISH} />);

describe('<AccountDetailsStep2 />', () => {
  it('renders properly', () => {
    const root = getRootComponent();
    expect(root.find('Grid Grid[item=true]').text()).toEqual(step2Content);
  });
});
