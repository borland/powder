import MaterialCardHeader from '@material-ui/core/CardHeader';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  cardHeader: {
    backgroundColor: theme.palette.grey['200']
  }
});

@withStyles(styles)
export default class CardHeader extends Component {
  static propTypes = {
    classes: PropTypes.shape({
      cardHeader: PropTypes.string.isRequired
    }).isRequired
  };

  render() {
    const { classes, ...rest } = this.props;
    return <MaterialCardHeader className={classes.cardHeader} {...rest} />;
  }
}
