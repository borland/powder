export ConsoleMock from './console';
export FetchMock, { requestFailure, withAuthRequestHeader } from './fetch';
export LocalStorageMock from './localStorage';
export TestRouter from './testRouter';
export { historyMock, matchMock } from './router';
