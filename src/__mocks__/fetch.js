import { jwt } from 'mocks/constants';

export const requestFailure = TypeError('Failed to fetch');

export default class FetchMock {
  constructor(responseData, { fail = false, withAuthResponseHeader = false } = {}) {
    this.responseData = responseData || { status: 'ok' };
    this.withAuthResponseHeader = withAuthResponseHeader;
    this.windowFetch = window.fetch;
    this.fn = jest.fn(this.jestFn(fail));
    window.fetch = this.fn;
  }

  deactivate() {
    window.fetch = this.windowFetch;
  }

  getResponseData = (url, resolve) =>
    (typeof this.responseData === 'function' ? this.responseData(url, resolve) : this.responseData);

  jestFn = fail => url => new Promise((resolve, reject) => {
    const resolver = () => {
      const results = this.getResponseData(url);
      return typeof (results || {}).then === 'function'
        ? results
        : new Promise(_resolve => { _resolve(results); });
    };
    const response = {
      headers: { get: name => (this.withAuthResponseHeader && name === 'Authorization' ? `Bearer ${jwt}` : '') },
      json: resolver,
      text: resolver
    };
    if (fail) {
      reject(requestFailure);
    } else {
      resolve(response);
    }
  });
}

export const withAuthRequestHeader =
  options => _.set(options, 'headers', { Authorization: `Bearer ${localStorage.getItem('jwt')}` });
